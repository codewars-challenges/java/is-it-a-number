package be.hics.sandbox.isitanumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsItANumberApplication {

    public static void main(String[] args) {
        SpringApplication.run(IsItANumberApplication.class, args);
    }
}
