package be.hics.sandbox.isitanumber;

import java.util.regex.Pattern;

public class MyUtilities {

    public boolean isDigit(String s) {
        System.out.println(s);
        Pattern pattern = Pattern.compile("^\\s?-?(\\d+)(\\.\\d+)?\\s?$");
        return pattern.matcher(s).matches();
    }

}
